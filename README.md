# Aiguilles Discord Bot

To install dependencies:

```bash
bun install
```

To run:

```bash
bun run index.ts
```

Before running the bot, remember to deploy the slash commands to the guild:

```bash
bun run deploy-commands.ts
```

You will also need to set your tokens and IDs in the `.env.example` file. You then need to rename it to `.env`.

This project was created using `bun init` in bun v1.0.15. [Bun](https://bun.sh) is a fast all-in-one JavaScript runtime.

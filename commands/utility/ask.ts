import type { Interaction } from "discord.js";

const OpenAI = require("openai");
const { SlashCommandBuilder } = require("discord.js");

const openai = new OpenAI({
    apiKey: process.env.OPENAI_API_KEY, // This is the default and can be omitted
});

module.exports = {
    data: new SlashCommandBuilder()
        .setName("ask")
        .setDescription("Posez une question au chatbot Aiguilles.")
        .addStringOption((option) =>
            option
                .setName("question")
                .setDescription("Votre question.")
                .setRequired(true),
        ),
    async execute(interaction: Interaction) {
        const question =
            (interaction.options.getString("question") as string) || "";
        try {
            await interaction.deferReply();
            const chatCompletion = await openai.chat.completions.create({
                messages: [
                    {
                        role: "system",
                        content:
                            "Vous êtes un assistant utile qui répond de manière succincte. Vous ne vous addressez pas au client par 'client' ou par 'utilisateur', dites 'vous'. Restez Formel. Vous Travaillez pour la marque 'Aiguilles'. Aiguilles est une marque de montres de luxe hautement personnalisables. Il n'existe qu'un seul modèle de montre: Aiguilles One. Le client peut commencer à personnaliser sa montre sur le site web (https://aiguilles.azurewebsites.net). Le client peut commencer à personnaliser sa montre sur le site: Bracelet: or, argent; Boitier (boitier, lunette, couronne, dos): or, argent; Cadran: émeraude, lapis-lazuli, spinelle. Il devra ensuite aller en boutique pour rencontrer un horloger et choisir le reste de ses personnalisations. La seule boutique Aiguilles se trouve au 5 Rue Sainte-Catherine, 33000 Bordeaux, France.",
                    },
                    { role: "user", content: question },
                ],
                model: "gpt-3.5-turbo",
            });
            console.log(question);

            const content = (chatCompletion.choices[0] || { message: "" })
                .message;
            await interaction.editReply(content);
        } catch (err) {
            await interaction.editReply("As an AI robot, I errored out.");
        }
    },
};
